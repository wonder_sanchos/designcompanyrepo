﻿using System.Collections.Generic;
namespace ProjectLibrary.Model
{
    public enum StageId : int
    {
        Finished = 1,
        InProgress = 2,
        OnExamination = 3,
        OnChange = 4,
        RemovalOfComments = 5,
        Canceled = 6
    }
    public class Stage
    {
        public StageId StageId { get; set; }
        public string Name { get; set; }
        public ICollection<Project> Projects { get; set; } = new HashSet<Project>();
    }
}
