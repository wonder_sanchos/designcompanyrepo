﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ProjectLibrary.Model
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string CustomerName { get; set; }
        [Required]
        public double Finances { get; set; }
        public ICollection<Project> Projects { get; set; } = new HashSet<Project>();
        
        public int? ContactsId { get; set; }
        [ForeignKey("ContactsId")]
        public Contacts Contacts { get; set; }
    }
}
