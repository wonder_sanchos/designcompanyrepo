﻿using System.Collections.Generic;
namespace ProjectLibrary.Model
{
    public enum PositionId : int
    {
        Technician = 1,
        Engineer = 2,
        LeadEngineer = 3,
        GroupLeader = 4,
        ChiefSpecialist = 5,
        ProjectManager = 6,
        DeputyChief = 7,
        ChiefDepartment = 8
    }
    public class Position
    {
        public PositionId PositionId { get; set; }
        public string Name { get; set; }
        public ICollection<Employee> Employees { get; set; } = new HashSet<Employee>();
    }
}
