﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectLibrary.Model
{
    public class Project
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(150)]
        public string ProjectName { get; set; }
        [Required]
        [MaxLength(50)]
        public string BuildingName { get; set; }
        [Required]
        [MaxLength(30)]
        public string ProjectCipher { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        
        public StageId StageId { get; set; }
        [ForeignKey("StageId")]
        [Required]
        public Stage Stage { get; set; }

        [Required]
        public double Financing { get; set; }
        public ICollection<Employee> Participants { get; set; } = new HashSet<Employee>();

        public int? DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public Department DepartmentDeveloper { get; set; }
        
        [Required]
        [MaxLength(50)]
        public int? CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }
    }
}
