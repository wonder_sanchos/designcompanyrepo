﻿using System.ComponentModel.DataAnnotations;

namespace ProjectLibrary.Model
{
    public class Contacts
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Address { get; set; }
        [Required]
        [MaxLength(30)]
        public string PhoneNumber { get; set; }
        [Required]
        [MaxLength(30)]
        public string Email { get; set; }
    }
}
