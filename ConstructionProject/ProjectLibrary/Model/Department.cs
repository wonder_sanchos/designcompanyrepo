﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace ProjectLibrary.Model
{
    public class Department
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string DepartmentName { get; set; }
        [Required]
        [MaxLength(10)]
        public string ShortName { get; set; }

        public int? EmploeeId { get; set; }
        [ForeignKey("EmploeeId")]
        public ICollection<Employee> DepartmentMembers { get; set; } = new HashSet<Employee>();

        public int? ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public ICollection<Project> Projects { get; set; } = new HashSet<Project>();
    }
}
