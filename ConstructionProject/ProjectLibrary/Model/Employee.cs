﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectLibrary.Model
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        [MaxLength(20)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(20)]
        public string LasttName { get; set; }
        [Required]
        [MaxLength(20)]
        public string Patronomic { get; set; }
        
        public PositionEnum PositionId { get; set; }
        [ForeignKey("PositionId")]
        [Required]
        public Position Position { get; set; }

        [Required]
        public DateTime HireDate { get; set; }
        public ICollection<Project> Projects { get; set; } = new HashSet<Project>();
        
        public int? DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }
        
        public int? ContactId { get; set; }
        [ForeignKey("ContactId")]
        public Contacts Contacts { get; set; }
    }
}
