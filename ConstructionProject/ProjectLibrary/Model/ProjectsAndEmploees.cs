﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ProjectLibrary.Model
{
    public class ProjectsAndEmploees
    {
        [Key]
        public int ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public Project Project { get; set; }

        public int? EmploeeId { get; set; }
        [ForeignKey("EmploeeId")]
        public Employee Emploee { get; set; }

        public double PercentsOfProject { get; set; }
    }
}
