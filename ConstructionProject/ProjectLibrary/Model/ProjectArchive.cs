﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace ProjectLibrary.Model
{
    public class ProjectArchive
    {
        [Key]
        public int Id { get; set; }
        public int? ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        [Required]
        public Project Project { get; set; }
        [Required]
        public DateTime ArchiveDate { get; set; }
        [Required]
        [MaxLength(50)]
        public string Reason { get; set; }
    }
}
