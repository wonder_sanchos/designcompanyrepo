﻿using System;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using ProjectLibrary.Model;

namespace ProjectLibrary
{
    public class BuildingProjectDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Contacts> Contacts { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Stage> Stages { get; set; }
        public DbSet<ProjectsAndEmploees> ProjectsAndEmploees { get; set; }
        public DbSet<ProjectArchive> ProjectArchive { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            string connectionString = config.GetConnectionString("DefaultConnection");

            optionsBuilder.UseSqlServer(connectionString);

            optionsBuilder.LogTo(message => System.Diagnostics.Debug.WriteLine(message));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Employee>()
                .Property(position => position.PositionId)
                .HasConversion<int();

            modelBuilder
                .Entity<Position>()
                .Property(position => position.PositionId)
                .HasConversion<int>();

            modelBuilder
                .Entity<Position>()
                .HasData(
                Enum.GetValues(typeof(PositionId))
                .Cast<PositionId>()
                .Select(positionId => new Position()
                {
                    PositionId = positionId,
                    Name = positionId.ToString()
                })
                );



            modelBuilder
                .Entity<Project>()
                .Property(stage => stage.StageId)
                .HasConversion<int>();

            modelBuilder
                .Entity<Stage>()
                .Property(stage => stage.StageId)
                .HasConversion<int>();

            modelBuilder
                .Entity<Stage>()
                .HasData(
                Enum.GetValues(typeof(StageId))
                .Cast<StageId>()
                .Select(stageId => new Stage()
                {
                    StageId = stageId,
                    Name = stageId.ToString()
                })
                );


            modelBuilder.Entity<Department>().HasAlternateKey(dep => dep.ShortName);
            modelBuilder.Entity<Project>().HasAlternateKey(proj => proj.ProjectCipher);
            modelBuilder.Entity<Project>().HasIndex(proj => new { proj.ProjectName, proj.BuildingName });
        }
    }
}
