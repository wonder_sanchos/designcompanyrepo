﻿using System;
using ProjectLibrary.Model;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLibrary.DAL
{
    public class ProjectRepository : Repository<Project>
    {
        public ProjectRepository(BuildingProjectDbContext context) : base(context) { }

        public override void Delete(Project entity)
        {
            var canceled = entity;
            canceled.Stage.StageId = StageId.Canceled;
            _context.Update(canceled);
            _context.Add(canceled);
        }
    }
}
