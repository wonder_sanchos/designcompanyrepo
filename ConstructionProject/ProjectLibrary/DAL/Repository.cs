﻿using System;
using ProjectLibrary.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLibrary.DAL
{
    public class Repository<T> : IRepository<T>
        where T : class
    {
        protected readonly BuildingProjectDbContext _context;

        public Repository(BuildingProjectDbContext context)
        {
            _context = context;
        }

        public virtual void Delete(T entity)
        {
            _context.Remove<T>(entity);
        }

        public void Create(T entity)
        {
            _context.Add(entity);
        }

        public T Read(int id)
        {
            return _context.Find<T>(id);
        }

        public IQueryable<T> ReadAll()
        {
            return _context.Set<T>();
        }

        public T Update(T entity)
        {
            return _context.Update(entity).Entity;
        }
    }
}
