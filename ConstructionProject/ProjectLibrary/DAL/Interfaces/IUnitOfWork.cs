﻿using System;
using ProjectLibrary.Model;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLibrary.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        public ProjectRepository Projects { get; }
        public Repository<Employee> Employees { get; }
        public void SaveChanges();
    }
}
