﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLibrary.DAL.Interfaces
{
    public interface IRepository<T>
        where T : class
    {
        public T Read(int id);
        public IQueryable<T> ReadAll();
        public void Delete(T entity);
        public void Create(T entity);
        public T Update(T entity);
    }
}
