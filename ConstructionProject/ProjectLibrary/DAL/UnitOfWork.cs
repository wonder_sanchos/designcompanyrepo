﻿using System;
using ProjectLibrary.Model;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLibrary.DAL
{
    public class UnitOfWork
    {
        public readonly BuildingProjectDbContext _context;
        
        private ProjectRepository _projects;
        public ProjectRepository Projects => _projects ??= new ProjectRepository(_context);

        public Repository<Employee> Employees => new Repository<Employee>(_context);


        public UnitOfWork(BuildingProjectDbContext context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
