﻿using System;
using ProjectLibrary.Model;
using ProjectLibrary.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using ProjectServices.Interfaces;

namespace ProjectServices
{
    public class ProjectServices : IProjectServices
    {
        private readonly IUnitOfWork _uow;
        public ProjectServices(IUnitOfWork uow)
        {
            _uow = uow;
        }
        
        public List<Employee> GetNewcomers()
        {
            return
             _uow.Employees
                .ReadAll()
                .Where(u => u.HireDate > DateTime.Now.AddMonths(-1))
                .ToList();
        }

        public void CancelProject(Project project)
        {
            ICollection<Employee> participants = FindProjectParticipants(project);

            foreach (var employee in participants)
            {
                _uow.Employees.Delete(employee);
            }
            _uow.Projects.Delete(project);
            _uow.SaveChanges();
        }

        public List<Employee> FindProjectParticipants(Project project)
        {
            List<Employee> participants = new List<Employee>();
            foreach(var employee in _uow.Employees.ReadAll())
            {
                foreach(var proj in employee.Projects)
                {
                    if(proj.ProjectCipher == project.ProjectCipher)
                    {
                        participants.Add(employee);
                    }
                }
            }
            return participants;
        }

        public void EditProject(Project project)
        {
            _uow.Projects.Update(project);
            _uow.SaveChanges();
        }

        public Project GetProject(Project project)
        {
            return _uow.Projects.Read(project.Id);
        }

        public int CountAllPartisipants(Project project)
        {
            return _uow.Projects.ReadAll().Count();
        }

        public bool IsDeadLine(Project project)
        {
            return _uow.Projects.Read(project.Id).FinishDate < DateTime.Now;
        }
    }
}
