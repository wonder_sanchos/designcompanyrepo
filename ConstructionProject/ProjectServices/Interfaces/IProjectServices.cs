﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectLibrary.Model;
using System.Text;
using System.Threading.Tasks;

namespace ProjectServices.Interfaces
{
    public interface IProjectServices
    {
        public List<Employee> GetNewcomers();
        public void CancelProject(Project project);
        public List<Employee> FindProjectParticipants(Project project);
        public void EditProject(Project project);
        public int CountAllPartisipants(Project project);
        public bool IsDeadLine(Project project);
    }
}
